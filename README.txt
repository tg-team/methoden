Willkommen in der teamGLOBAL Methodenbibliothek!
____________________________________________________________________________

Hier findest du alles, was du benötigst, um tolle Einsätze zu gestalten.

Im Ordner "Bib" befinden sich alle Methoden, nach Themen geordnet.
Du kannst auch in der Suchzeile nach Stichwörtern suchen, um in die richtigen Verzeichnisse zu gelangen. 

Oder du besuchst die Website: https://tg-team.gitlab.io/methoden/, um dir alle Infos und Methodensteckbriefe übersichtlich anzeigen zu lassen.
