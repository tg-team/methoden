ANLEITUNG ZUR DIREKTEN MITARBEIT AN DER teamGLOBAL METHODENBIBLIOTHEK
______________________________________________________________________

Jedes Teammitglied kann mitwirken, in dem es 
- Dateien oder Ordner hochlädt,
- Dateien online bearbeitet oder
- Anmerkungen (Issues) hinzufügt. 

Dafür ist praktisch keine Kenntnis von git nötig, alles kann im Browser erledigt werden.
______________________________________________________________________

Die Mitglieder der AG Methoden, die das Verzeichnis und die zugehörige
Präsentationsseite verwalten, sollten jedoch mit git vertraut sein und die
Standardabläufe der verteilten Entwicklung durchführen können. Es sind folgende
Schritte nötig:

1. Einen kostenlosen Account auf gitlab.com registrieren.

2. Auf dem Computer, den man benutzt, ein Schlüsselpaar zur sicheren
Kommunikation mit gitlab erstellen
(Anleitung: https://docs.gitlab.com/ce/ssh/README.html)
und den Schlüssel online unter https://gitlab.com/profile/keys hinzufügen.

3. Das Git-Programm von https://git-scm.com/downloads für das eigene
Betriebssystem herunterladen und installieren.

4. Von der Methoden-Projektseite auf gitlab: https://gitlab.com/tg-team/methoden
den SSH-Link des Repositoriums kopieren: "git@gitlab.com:tg-team/methoden.git"

5. Die Git-GUI auf dem eigenen PC starten, auf "Clone Existing Repository"
klicken und dann unter "Source Location" den SSH-Link aus Schritt 4 einfügen,
unter "Target Directory" ein neues Verzeichnis auswählen, in das die Methodenbibliothek kopiert werden soll. Dann wird die Methodenbibliothek synchronisiert.

Für den normalen Arbeitsablauf sollte man mit den Standard-Git-Befehlen
- add bzw. stage changed (Änderungen eintragen),
- commit (eingetragene Änderungen zu einem "Update-Paket" bündeln) und
- push (Update-Packet ins Online-Verzeichnis für alle hochladen

sowie 

- fetch (Änderungen von anderen abrufen) und
- merge (Änderungen von anderen mit den eigenen Änderungen vereinigen)

vertraut sein. Zum Ausführen dieser
Befehle kann ausschließlich das Programm Git GUI verwendet werden.

